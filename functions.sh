
function trim {
	echo $@ | sed -e 's/^[[:blank:]]*//' | sed -e 's/[[:blank:]]*$//'
}

function print_border {
	for i in `seq 1 $1`
	do
		printf "*"
	done
	printf "\n"
}

function exec_cmd_list {
	local cmd_file=$1
	local should_wait=$2

	while read -u 3 line || [ -n "$line" ]
	do
		if [[ $line =~ ^[:space:]*$ ]]
		then
			continue
		fi

		eval $line
		if $should_wait
		then
			printf "\n"
			read -p "Press return to continue..." -e -s
			printf "\n\n"
		fi
	done 3< $cmd_file
}
