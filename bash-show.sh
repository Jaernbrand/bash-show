#!/usr/bin/env bash

# Reads the commands in a specified file and executes the commands one at a 
# time, awaiting user input between each command. Takes one config file as 
# argument. The config file consists of attributes followed by an equals sign
# and the value of the attribute. See the example config file in the `examples`
# directory.
#
# The config file may contain the following properties:
#
# * project_name - The name of the project. Optional.
#
# * main_cmds - File containing comands to be executed. Mandatory.
#
# * init_cmds - File containing comands to be executed. Optional.
#
# * clean_cmds - File containing comands to be executed. Optional.
#
# Only the main_cmds property is mandatory. Everything that isn't one of the
# properties above will be ignored. The value of a property may not be commented 
# out. The following `main_cmds = #cmd_file` will result in the value 
# `#cmd_file` being assigned to `main_cmds`.
#
# Example usage:
# ./bash-show.sh my_conf_file 


if [[ -z $1 ]]
then
	printf "Expected config file as argument\n" >&2
	exit 1
elif [[ -n $2 ]]
then
	printf "Only one config file may be supplied\n" >&2
	exit 1
fi

config_file=$1

root_dir=$(dirname $0)
source $root_dir/functions.sh

# Read config file and extract the command lists
while IFS='=' read -a line || [ -n "$line" ]
do
	key=$(trim ${line[0]})
	case $key in
		"project_name")
			project_name=$(trim ${line[@]:1})
			;;

		"main_cmds")
			main_cmds=$(trim ${line[1]})
			;;

		"init_cmds")
			init_cmds=$(trim ${line[1]})
			;;

		"clean_cmds")
			clean_cmds=$(trim ${line[1]})
			;;
	esac
done < $config_file

# Make sure that $main_cmds is set.
if [[ -z $main_cmds ]]
then
	printf "'main_cmds' property may not be empty\n" >&2
	exit 1
fi

exec_cmd_list $init_cmds false
printf "\nInit done\n\n"

print_border ${#project_name}
printf "$project_name\n"
print_border ${#project_name}

exec_cmd_list $main_cmds true
printf "Done. Starting cleanup...\n"

exec_cmd_list $clean_cmds false
printf "\nCleanup done\n\n"

