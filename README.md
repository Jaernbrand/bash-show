# bash-show

Reads the commands in a specified file and executes the commands one at a 
time, awaiting user input between each command. Takes one config file as 
argument. The config file consists of attributes followed by an equals sign
and the value of the attribute. See the example config file in the `examples`
directory.

The config file may contain the following properties:
* project_name - The name of the project. Optional.
* main_cmds - File containing comands to be executed. Mandatory.
* init_cmds - File containing comands to be executed. Optional.
* clean_cmds - File containing comands to be executed. Optional.

Only the main_cmds property is mandatory. Everything that isn't one of the
properties above will be ignored. The value of a property may not be commented 
out. The following `main_cmds = #cmd_file` will result in the value 
`#cmd_file` being assigned to `main_cmds`.

## Example usage:
```
./bash-show.sh my_conf_file 
```
